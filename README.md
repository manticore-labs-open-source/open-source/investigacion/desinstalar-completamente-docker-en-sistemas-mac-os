# Desinstalar completamente Docker en sistemas Mac OS

## Docker en Mac OS

Debido a los lanzamientos continuos de actualizaciones del sistema operativo con el que cuenta Mac OS, existen dos versiones de docker para el sistema: 
* La versión "convencional" que sólo necesita descargarse y arrastrarse en la carpeta *_Aplicaciones_* para que funcione
* La versión **Toolbox** para las versiones más antiguas del sistema (inferiores a 10.9) cuya instalación se realiza a través de un wizard. Las versiones disponibles de Docker Toolbox para Mac OS, pueden encontrarse [aquí](https://github.com/docker/toolbox/releases).

Las versiones más antiguas del software (es decir, las versiones *_Toolbox_*) del sistema, tienen un problema en común: al momento de desinstalar de la manera tradicional que tiene Mac OS (eliminando las aplicaciones arrastrándolas a la papelera de reciclaje) no basta para eliminar completamente los contenedores creados. 

## Aclaraciones

* La presente guía funcionará sólo si se ha utilizado el instalador oficial de Docker para Mac OS. En caso de haberse instalado a través de *_Homebrew_* u otros, no resultará efectivo.
* La guía funcionará únicamente para la versión **Toolbox** de Docker.
* Los comandos a utilizar pueden encontrarse [aquí](https://github.com/docker/toolbox/blob/master/osx/uninstall.sh)

## Pasos para la desinstalación

* En primer, es necesario desinstalar todas las máquinas docker creadas a través del siguiente comando:
```
docker-machine rm -f $(docker-machine ls -q);
```
* Luego, es necesario remover la app a través del comando:
```
sudo rm -rf /Applications/Docker
```
* Una vez eliminada la app, es necesario eliminar los binarios así:
```
sudo rm -f /usr/local/bin/docker
sudo rm -f /usr/local/bin/boot2docker
sudo rm -f /usr/local/bin/docker-machine
sudo rm -r /usr/local/bin/docker-machine-driver*
sudo rm -f /usr/local/bin/docker-compose
```
* Los paquetes docker también deben ser eliminados:
```
sudo pkgutil --forget io.docker.pkg.docker
sudo pkgutil --forget io.docker.pkg.dockercompose
sudo pkgutil --forget io.docker.pkg.dockermachine
sudo pkgutil --forget io.boot2dockeriso.pkg.boot2dockeriso
```
* Finalmente, debe eliminarse la carpeta de configuración así:
```
rm -rf ~/.docker
```

**Nota:** En caso de haber utilizado la variable de entorno *_DOCKER_HOST_* en alguna otra ubicación, debe ser eliminada también.

## Desinstalación opcional

De manera opcional, se puede desinstalar *_VirtualBox_* pues es instalado por defecto para cargar los contenedores al momento de instalar Docker.

<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>